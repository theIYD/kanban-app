export enum ColumnStatus {
  REJECTED = "rejected",
  APPLIED = "applied",
  SHORTLISTED = "shortlisted",
}

export type columnType = "rejected" | "applied" | "shortlisted";

export const columns = [
  {
    headingColor: "border-red-100",
    backgroundColor: "bg-red-100",
    title: "Rejected",
    icon: "/assets/icons/rejected.svg",
    textColor: "text-[#EB5757]",
    column: "rejected",
  },

  {
    backgroundColor: "bg-gray-200",
    headingColor: "border-gray-200",
    title: "Applied",
    icon: "/assets/icons/applied.svg",
    textColor: "text-gray-800",
    column: "applied",
  },
  {
    headingColor: "border-green-200",
    backgroundColor: "bg-green-200",
    title: "Shortlisted",
    icon: "/assets/icons/shortlisted.svg",
    textColor: "text-green-600",
    column: "shortlisted",
  },
];

export interface EachCard {
  id: string;
  external?: boolean;
  name: string;
  image?: string;
  description?: string;
  verified?: boolean;
  column: columnType;
  rejected_by?: string;
  rejected_on?: string;
  rejected_by_img?: string;
  experience?: string;
  offer?: boolean;
  resume?: string;
  notice?: string;
  shortlisted_by?: string;
  shortlisted_on?: string;
  shortlisted_by_img?: string;
  email?: string;
  phone?: string;
  applied: string;
  ref_by?: string;
  refImg?: string;
}

export interface EachCardPage extends EachCard {
  handleDragStart: any;
}
