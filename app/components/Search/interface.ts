import { EachCard } from "@/app/mock";

export interface SearchProps {
  cards: EachCard[];
  setSearchCard: any;
}
