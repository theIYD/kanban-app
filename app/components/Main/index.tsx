"use client";
import React from "react";
import Header from "../Header";
import Kanban from "../Kanban";

const Main = () => {
  return (
    <div className="lg:ml-64 max-w-[100vw] pr-4">
      <div className="border-r border-l border-solid border-gray-200 max-w-[100%] h-full text-[#0D0D0D]">
        <Header />
        <Kanban />
      </div>
    </div>
  );
};

export default Main;
