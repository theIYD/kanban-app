"use client";
import React, { useEffect, useState } from "react";
import Search from "../Search";
import Column from "./Column";
import { EachCard, columns } from "@/app/mock";
import { MockCards } from "@/app/mock/data";

const Kanban = () => {
  const [cards, setCards] = useState<EachCard[]>([...MockCards]);
  const [searchCards, setSearchCard] = useState<EachCard[]>([...cards]);

  useEffect(() => {
    setSearchCard([...cards]);
  }, [cards]);

  return (
    <div className="px-4 py-6 w-full">
      <Search cards={cards} setSearchCard={setSearchCard} />
      <div className="flex mb-[5vh] gap-3 w-full">
        {columns.map((col, index) => (
          <Column
            key={index}
            {...col}
            searchCards={searchCards}
            cards={cards}
            setCards={setCards}
          />
        ))}
      </div>
    </div>
  );
};

export default Kanban;
