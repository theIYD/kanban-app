export type ColumnProps = {
  headingColor: string;
  backgroundColor: string;
  title: string;
  textColor: string;
  column: string;
  icon: string;
  cards: any;
  setCards: any;
  searchCards: any;
};
