"use client";
import React from "react";
import Image from "next/image";
import { ColumnProps } from "./interface";
import { EachCard } from "@/app/mock";
import Card from "../Card";
import { useDragDrop } from "@/app/hooks/useDragDrop";

const Column = ({
  headingColor,
  backgroundColor,
  title,
  textColor,
  icon,
  column,
  cards,
  setCards,
  searchCards,
}: ColumnProps) => {
  const {
    handleDragOver,
    handleDragLeave,
    handleDrop,
    handleDragStart,
    activeColumn,
  } = useDragDrop(setCards, cards, column, title);

  let filteredCards = searchCards.filter(
    (card: EachCard) => card.column === column
  );

  return (
    <div
      onDrop={handleDrop}
      onDragOver={handleDragOver}
      onDragLeave={handleDragLeave}
      className={`flex-grow shrink-0 relative w-[306px] ${
        activeColumn ? "bg-stone-200" : "bg-[#FAFBFC]"
      }  h-screen 
      transition-colors overflow-y-auto overflow-x-hidden rounded-md border border-solid  ${headingColor}`}
    >
      <div
        className={` w-full  sticky left-0 top-0 gap-2 flex rounded-t p-2 ${backgroundColor} items-center`}
      >
        <Image src={icon} alt={title} width={16} height={16} />
        <h3
          className={`font-semibold ${textColor} text-[12px] leading-[16px]`}
        >{`${title.toUpperCase()} • ${filteredCards.length} `}</h3>
      </div>

      {filteredCards.map((c: React.JSX.IntrinsicAttributes & EachCard) => {
        return <Card key={c.id} {...c} handleDragStart={handleDragStart} />;
      })}
      <div
        data-before={"-1"}
        data-col={column}
        className="my-0.5 h-7 min-w-full opacity-0"
      ></div>
    </div>
  );
};

export default Column;
