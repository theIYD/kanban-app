import Main from "./components/Main";
import Sidebar from "./components/Sidebar";

export default function Home() {
  return (
    <main className="flex w-full min-h-screen bg-white justify-center">
      <div className="lg:mx-[4vw] mx-0">
        <Sidebar />
        <Main />
      </div>
    </main>
  );
}
