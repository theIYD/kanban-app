/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    remotePatterns: [
      {
        protocol: "https",
        hostname: "www.vhv.rs",
        port: "",
      },
    ],
  },
};

export default nextConfig;
