# Kanban App

Hosted on: https://kanban-app-seven-bay.vercel.app/

### Details

- Used mock data to display the candidate cards present in `app/mock`.
- Added drag and drop functionality by writing a custom hook for a basic version. A 3rd party library like `react-dnd, dnd-kit` could be used from a maintenance perspective.
- Search across the candidates in the board.
- Used `framer-motion` for micro-interactions.
- Added icons from `react-icons` for ease.

### Tech stack

- Next.js, Tailwind, Framer
